import AppBar from "@material-ui/core/AppBar";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import axios from "axios";
import React, { Component } from "react";
import "./index.css";

const TableHeadings = [
  { id: "0", value: "first_name", display: "First Name" },
  { id: "1", value: "last_name", display: "Last Name" },
  { id: "2", value: "company_name", display: "Company Name" },
  { id: "3", value: "city", display: "City" },
  { id: "4", value: "state", display: "State" },
  { id: "5", value: "zip", display: "Zip" },
  { id: "6", value: "email", display: "Email" },
  { id: "7", value: "web", display: "Web" },
  { id: "8", value: "age", display: "Age" }
];

const maxRecords = 5;

export default class TableView extends Component {
  state = {
    tableData: [],
    tableAllData: [],
    tableDataSorted: [],
    currentIndex: 1,
    startIndex: 0,
    endIndex: 5,
    isSorted: false,
    message: false
  };

  componentWillMount = () => {
    let url = `https://datapeace-storage.s3-us-west-2.amazonaws.com/dummy_data/users.json`;

    axios.get(url).then(res => {
      this.setState({
        tableData: res.data,
        tableAllData: res.data
      });
    });
  };

  //view handler
  HandleViewDetails = id => {
    let findObj = this.state.tableData.find(x => x.id === id);
    this.props.history.push({ pathname: `/user/${id}`, state: findObj });
    console.log(findObj, "findObj");
  };

  //search handler
  handleChangeSearch = event => {
    if (event.target.value !== "") {
      let value =
        event.target.value[0].toUpperCase() + event.target.value.slice(1);
      this.filterNames(value);
    } else {
      this.setState({
        tableData: this.state.tableAllData,
        isSorted: false
      });
    }
  };

  //filterNames filter handler
  filterNames = inputValue => {
    let sortedData = this.state.tableAllData.filter(item =>
      item.first_name.includes(inputValue)
    );
    sortedData.length > 0
      ? this.setState({
          tableData: sortedData,
          tableDataSorted: sortedData,
          isSorted: true,
          currentIndex: 1,
          startIndex: 0,
          endIndex: 5,
          message: false
        })
      : this.setState({
          tableData: sortedData,
          tableDataSorted: sortedData,
          isSorted: true,
          message: true
        });
  };

  // pagination handler
  handlePagination = (isSorted, actionType) => {
    let useTableData = [];
    isSorted
      ? (useTableData = this.state.tableDataSorted)
      : (useTableData = this.state.tableAllData);
    let paginationOperation = {
      prev: value => {
        value.startIndex =
          value.startIndex >= maxRecords
            ? value.startIndex - maxRecords
            : value.startIndex;
        value.endIndex =
          value.endIndex >= 2 * maxRecords
            ? value.endIndex - maxRecords
            : value.endIndex;
        value.currentIndex = value.currentIndex - 1;

        value.currentIndex = value.currentIndex || 1;
        return value;
      },

      next: value => {
        let maxIncrement = Math.ceil(
          useTableData.length / maxRecords > value.currentIndex
        );

        value.startIndex = maxIncrement ? value.endIndex : value.startIndex;
        value.endIndex = maxIncrement
          ? value.endIndex + maxRecords
          : value.endIndex;
        value.currentIndex = maxIncrement
          ? (value.currentIndex += 1)
          : value.currentIndex;
        return value;
      }
    };
    let opeartion = paginationOperation[actionType](this.state);
    let data = useTableData;
    let dataSliced = data.slice(opeartion.startIndex, opeartion.endIndex);
    this.setState({
      tableData: dataSliced,
      currentIndex: opeartion.currentIndex,
      startIndex: opeartion.startIndex,
      endIndex: opeartion.endIndex
    });
  };

  //sorting handler
  handleSortColumn = (tableToSort, sortBy) => {
    let operationSort = {
      [sortBy]: value => {
        value = value.sort((a, b) =>
          a[sortBy].toString().localeCompare(b[sortBy])
        );
        return value;
      }
    };
    let operation = operationSort[sortBy](tableToSort);
    this.setState({
      tableData: operation
    });
  };

  render() {
    let {
      tableData,
      tableDataSorted,
      tableAllData,
      isSorted,
      message
    } = this.state;

    return (
      <div className="table-container">
        <AppBar position="static">
          <Toolbar>
            <IconButton
              edge="start"
              // className={classes.menuButton}
              color="inherit"
              aria-label="menu"
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="h6"
              // className={classes.title}
            >
              Data Peace
            </Typography>
          </Toolbar>
        </AppBar>
        <Paper elevation={0} className="data-paper">
          <div className="textfield-div">
            <TextField
              id="standard-name"
              label="Search by first name"
              // className={classes.textField}
              // value={values.name}
              onChange={this.handleChangeSearch}
              margin="normal"
            />
          </div>
          <table>
            <tbody>
              <tr>
                {TableHeadings.map((item, HIndex) => (
                  <th key={HIndex}>
                    <img
                      src="assets/sortDown.png"
                      alt="filter"
                      onClick={() =>
                        this.handleSortColumn(tableData, item.value)
                      }
                    />
                    {item.display}
                  </th>
                ))}
              </tr>
              {tableData &&
                tableData.map(
                  (itemTable, index) =>
                    index < 5 && (
                      <tr
                        onClick={() => this.HandleViewDetails(itemTable.id)}
                        key={index}
                      >
                        <td>{itemTable.first_name}</td>
                        <td>{itemTable.last_name}</td>
                        <td>{itemTable.company_name}</td>
                        <td>{itemTable.city}</td>
                        <td>{itemTable.state}</td>
                        <td>{itemTable.zip}</td>
                        <td>{itemTable.email}</td>
                        <td>{itemTable.web}</td>
                        <td>{itemTable.age}</td>
                      </tr>
                    )
                )}
            </tbody>
          </table>
          {message ? (
            <div>No Data Found</div>
          ) : (
            <React.Fragment>
              {isSorted ? (
                <div className="pagination">
                  <a
                    href="#"
                    onClick={() => this.handlePagination(isSorted, "prev")}
                  >
                    &laquo;
                  </a>
                  <a>
                    {`${this.state.currentIndex} of ${tableDataSorted.length >
                      0 && Math.ceil(tableDataSorted.length / maxRecords)}`}
                  </a>
                  <a
                    href="#"
                    onClick={() => this.handlePagination(isSorted, "next")}
                  >
                    &raquo;
                  </a>
                </div>
              ) : (
                <div className="pagination">
                  <a
                    href="#"
                    onClick={() => this.handlePagination(isSorted, "prev")}
                  >
                    &laquo;
                  </a>
                  <a>
                    {`${this.state.currentIndex} of ${tableAllData.length > 0 &&
                      Math.ceil(tableAllData.length / maxRecords)}`}
                  </a>
                  <a
                    href="#"
                    onClick={() => this.handlePagination(isSorted, "next")}
                  >
                    &raquo;
                  </a>
                </div>
              )}
            </React.Fragment>
          )}
        </Paper>
      </div>
    );
  }
}
