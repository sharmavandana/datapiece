import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import React, { Component } from "react";
import "./index.css";

const form = [
  {
    id: "rooms",
    display: "Rooms",
    icon: "/assets/hotel-bed.png",
    o: 1,
    operation: [
      {
        id: "subtract",
        icon: <RemoveIcon className="operationIconRemove" />,
        display: "Subtract"
      },
      {
        id: "add",
        icon: <AddIcon className="operationIconAdd" />,
        display: "Add"
      }
    ]
  },
  {
    id: "adults",
    display: "Adults",
    icon: "/assets/adult.png",
    o: 2,
    operation: [
      {
        id: "subtract",
        icon: <RemoveIcon className="operationIconRemove" />,
        display: "Subtract"
      },
      {
        id: "add",
        icon: <AddIcon className="operationIconAdd" />,
        display: "Add"
      }
    ]
  },
  {
    id: "children",
    display: "Children",
    icon: "/assets/child.png",
    o: 1,
    operation: [
      {
        id: "subtract",
        icon: <RemoveIcon className="operationIconRemove" />,
        display: "Subtract"
      },
      {
        id: "add",
        icon: <AddIcon className="operationIconAdd" />,
        display: "Add"
      }
    ]
  }
];

const roomConstant = 5;
const noOfPeople = 4;

class Cart extends Component {
  state = {
    rooms: 1,
    adults: 1,
    children: 0,
    disableAdd: false,
    disableSubtract: true
  };

  getRooms(req, operation) {
    let r = Math.ceil((req.adults + req.children) / noOfPeople);
    let rooms;
    let entityOperations = {
      add: () => {
        return r <= req.rooms ? req.rooms : r;
      },

      subtract: () => {
        let room =
          req.rooms > req.adults + req.children ? req.rooms - 1 : req.rooms;
        return r < req.rooms ? room : r;
      }
    };
    rooms =
      req.rooms <= roomConstant ? entityOperations[operation]() : req.rooms;
    return rooms;
  }

  handleChange = (entity, operation) => {
    // add
    let entityOperations = {
      add: {
        rooms: value => {
          let v = (value.rooms += 1);
          let people = value.adults + value.children;
          value.rooms = v <= roomConstant ? v : roomConstant;
          value.adults = people < value.rooms ? value.adults + 1 : value.adults;
          return value;
        },
        adults: value => {
          value.adults = value.adults += 1;
          return value;
        },
        children: value => {
          value.children = value.children += 1;
          return value;
        }
      },
      //subtract
      subtract: {
        rooms: value => {
          let v = Math.ceil((value.adults + value.children) / value.rooms);
          value.rooms = value.rooms -= 1;
          value.adults = v * value.rooms;
          value.children = 0;
          value.rooms = value.rooms || 1;
          value.adults = value.adults || 1;
          return value;
        },
        adults: value => {
          value.adults = value.adults -= 1;
          value.adults = value.adults || 1;
          return value;
        },
        children: value => {
          value.children = value.children -= 1;
          value.children = value.children || 0;
          return value;
        }
      }
    };

    let newValue = entityOperations[operation][entity](
      JSON.parse(JSON.stringify(this.state))
    );

    newValue = newValue[entity] >= 0 ? newValue : this.state;
    newValue.rooms =
      entity !== "rooms" ? this.getRooms(newValue, operation) : newValue.rooms;
    newValue.disableSubtract = newValue.rooms > 1 ? false : true;
    newValue.disableAdd =
      newValue.adults + newValue.children >= roomConstant * noOfPeople
        ? true
        : false;
    this.setState(newValue);
  };

  render() {
    return (
      <div className="cartComponent">
        <div>
          <div className="textDiv">
            <span>
              <img className="icon" alt="people" src="/assets/people.png" />
            </span>
            <span className="text">Choose number of People</span>
          </div>
          <Card>
            <CardContent className="card">
              {form.map((item, iIndex) => (
                <Grid container spacing={2} key={iIndex}>
                  <Grid item md={6}>
                    <div className="textDiv">
                      <span>
                        <img className="icon" alt="people" src={item.icon} />
                      </span>
                      <span className="text">{item.display}</span>
                    </div>
                  </Grid>
                  <Grid item md={6}>
                    <div className="buttonGroup">
                      {item.operation.map((operation, oIndex) => (
                        <>
                          <Button
                            variant="contained"
                            color="primary"
                            className="button"
                            disabled={
                              operation.id === "add"
                                ? this.state.disableAdd
                                : iIndex === 0 && this.state.disableSubtract
                            }
                            onClick={() =>
                              this.handleChange(item.id, operation.id)
                            }
                          >
                            {operation.icon}
                          </Button>
                          {oIndex === 0 ? (
                            <span className="text-entity">
                              {this.state[item.id]}
                            </span>
                          ) : null}
                        </>
                      ))}
                    </div>
                  </Grid>
                  <Grid item md={12}>
                    {iIndex !== 2 ? <hr /> : null}
                  </Grid>
                </Grid>
              ))}
            </CardContent>
          </Card>
        </div>
      </div>
    );
  }
}

export default Cart;
