import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Paper from "@material-ui/core/Paper";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import React from "react";
import "./index.css";
const DetailView = props => {
  let userDetails = props.location.state;
  console.log(props.location.state, "props");
  return (
    <div className="detail-view-container">
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu">
            <ArrowBackIcon
              onClick={() => {
                props.history.push({ pathname: `/` });
              }}
            />
          </IconButton>
          <Typography variant="h6">Data Peace</Typography>
        </Toolbar>
      </AppBar>
      <Paper elevation={0} className="detail-view-paper">
        <Grid container spacing={2}>
          <Grid item xs={6} className="left-div">
            <h2>{userDetails.first_name + " " + userDetails.last_name}</h2>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={6} className="left-div">
            Company
          </Grid>
          <Grid item xs={6} className="right-div">
            <div>{userDetails.company_name}</div>
          </Grid>
        </Grid>
        <hr />
        <Grid container spacing={2}>
          <Grid item xs={6} className="left-div">
            City
          </Grid>
          <Grid item xs={6} className="right-div">
            <div>{userDetails.city}</div>
          </Grid>
        </Grid>
        <hr />
        <Grid container spacing={2}>
          <Grid item xs={6} className="left-div">
            State
          </Grid>
          <Grid item xs={6} className="right-div">
            <div>{userDetails.state}</div>
          </Grid>
        </Grid>
        <hr />
        <Grid container spacing={2}>
          <Grid item xs={6} className="left-div">
            Zip
          </Grid>
          <Grid item xs={6} className="right-div">
            <div>{userDetails.zip}</div>
          </Grid>
        </Grid>
        <hr />
        <Grid container spacing={2}>
          <Grid item xs={6} className="left-div">
            Email
          </Grid>
          <Grid item xs={6} className="right-div">
            <div>{userDetails.email}</div>
          </Grid>
        </Grid>
        <hr />
        <Grid container spacing={2}>
          <Grid item xs={6} className="left-div">
            Web
          </Grid>
          <Grid item xs={6} className="right-div">
            <div>{userDetails.web}</div>
          </Grid>
        </Grid>
        <hr />
        <Grid container spacing={2}>
          <Grid item xs={6} className="left-div">
            Age
          </Grid>
          <Grid item xs={6} className="right-div">
            <div>{userDetails.age}</div>
          </Grid>
        </Grid>
        <hr />
      </Paper>
    </div>
  );
};

export default DetailView;
