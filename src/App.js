import React from "react";
import { Route, Switch } from "react-router-dom";
import "./App.css";
import DetailView from "./components/detail-view/index";
import TableView from "./components/table-view/index";

function App() {
  return (
    <div className="App">
      <main>
        <Switch>
          <Route path="/" exact component={TableView} />
          <Route path="/user" component={DetailView} />
        </Switch>
      </main>
    </div>
  );
}

export default App;
